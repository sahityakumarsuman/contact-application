/**
 * Controller layer
 *
 * Interacts with model and business logic for
 * the request goes here.
 */

import { promisify } from 'util';
import request, { Response } from 'request';
import { Guest, SentMesssages } from '../models';

import * as constants from '../utils/constants';
import { IGuestModel } from '../types/Models.d';

const [request_get, request_post, request_put] = [
  promisify(request.get),
  promisify(request.post),
  promisify(request.put)
];


export const findGuest = (user_id: string, projections?: object): Promise<IGuestModel> => {
  return Guest.findOne({_id: user_id}, projections ? projections : {})
      .then((user: IGuestModel) => {
        if (!user)
          return Promise.reject({
            err: `user does not exists`,
            message: `user does not exists!`
          });
        else return Promise.resolve(user);
      });
};


export const createUser = (name: string, profile_url: string, contact_number: string, email: string, abv_count: number) => {
  const new_user = {
    name: name,
    profile_url: profile_url,
    contact_number: contact_number,
    email: email,
    abv_count: abv_count
  };
  return Guest.create(new_user)
      .then((created_user) => {
        if (!created_user)
          return Promise.reject({
            err: `Failed to create user`,
            message: `User not created`
          });
        else return Promise.resolve(created_user);
      });
};


export const entry_sent_message = (sent_message_to: string, sent_message_from: string, message: string, sent_message_pic: string) => {
  const new_sent_message = {
    sent_message_to: sent_message_to,
    sent_message_from: sent_message_from,
    message: message,
    message_timestamp: Date.now(),
    sent_message_pic: sent_message_pic
  };
  return SentMesssages.create(new_sent_message)
      .then((created_doc) => {
        if (!created_doc)
          return Promise.reject({
            err: `Failed to create doc`,
            message: `Message sent not created`
          });
        else return Promise.resolve(created_doc);
      });
};


export const getAllUser = () => {
  return Guest.find()
      .then((all_user_list) => {
        if (!all_user_list)
          return Promise.reject({
            err: `nothing found in database`,
            message: `need to give some entry in db`
          });
        else return Promise.resolve(all_user_list);
      });
};

export const getAllSentMessages = () => {
  return SentMesssages.find()
      .then((all_sent_messages) => {
        if (!all_sent_messages)
          return Promise.reject({
            err: `No messages have been sent`,
            message: `Send some messages to show anything`
          });
        else return Promise.resolve(all_sent_messages);
      });
};


export const sendMessage = (contact_number: string, otp_message: number) => {
  const api = constants.TWO_FACTOR_APIS.SEND_OTP(contact_number, otp_message);
  return request_get({url: api, json: true}, undefined)
      .then((response: Response) => {
        // log the OTP sent response => `response.body`
        if (response.body.Status !== 'Success') {
          return Promise.reject({
            err: `OTP not sent`,
            message: `${response.body.Details}`
          });
        } else {
          console.log(`[sendMessage] sendMessage(${contact_number}, ${otp_message}) sent successfully!`);
          return Promise.resolve({
            success: true,
            message: 'message sent successfully'
          });
        }
      });
};

