
FROM mhart/alpine-node:5

WORKDIR /user-microservice
COPY . .

RUN npm install --production
RUN npm install -g pm2


EXPOSE 8079

                   