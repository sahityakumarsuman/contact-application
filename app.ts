import express from 'express';
import bodyParser from 'body-parser';
import logger from 'morgan';
import cors from 'cors';


import { users } from './routes';

const app = express();

app.set('json spaces', 4);
app.disable('etag');
app.use(cors());

if (process.env.NODE_ENV === 'development') {
  app.use(logger('dev'));
}

app.use(bodyParser.json({limit: '50mb', extended: false, type: 'application/json'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: false, parameterLimit: 50000}));

app.get('/health', (_, res) => res.status(200).json({}));

app.use('/user', users);

// routes


module.exports = app;

