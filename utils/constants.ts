const TWO_FACTOR_SERVER_ADDR: string = 'https://2factor.in/API/V1';
const TWO_FACTOR_API_KEY: string = process.env.TWO_FACTOR_API_KEY || '8b4c597a-c4a3-11e8-a895-0200cd936042';

const TWO_FACTOR_APIS = {
  SEND_OTP: (contact_number: string, otp: number) =>
      `${TWO_FACTOR_SERVER_ADDR}/${TWO_FACTOR_API_KEY}/SMS/${contact_number}/${otp}`
};


export {
  TWO_FACTOR_APIS
};

