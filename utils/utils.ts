export const getUnixTimeStamp = () => Math.floor(new Date().getTime() / 1000);

export const generateOTP = (): number => {
  const OTP_LENGTH: number = 4;
  let counter: number = 0;
  let result: number = 0;
  while (counter++ < OTP_LENGTH) {
    const randDigit: number = Math.floor(Math.random() * 10);
    if (randDigit == 0)
      result = result * 10 + 1;
    else result = result * 10 + randDigit;
  }
  return result;
};

export const getRandomNumber = (from: number, to: number): number => {
  return Math.floor((Math.random() * from) + to);
};

