import { generateOTP, getRandomNumber } from '../utils/utils';
import * as controller from '../controllers';
import chalk from 'chalk';
import { CustomError } from '../types/Interfaces';

import { Request, Response } from 'express';

const router = require('express').Router();


const profile_images = [
  'https://naldzgraphics.net/wp-content/uploads/2014/09/2-joker-icon.jpg',
  'http://images.naldzgraphics.net/2014/09/1-batman-icon.jpg',
  'http://images.naldzgraphics.net/2014/09/10-wolverine-icon.jpg',
  'https://naldzgraphics.net/wp-content/uploads/2014/09/18-thor-icons.jpg',
  'https://res.cloudinary.com/teepublic/image/private/s--a_NvDOQS--/t_Resized%20Artwork/c_fit,g_north_west,h_954,w_954/co_ffffff,e_outline:48/co_ffffff,e_outline:inner_fill:48/co_ffffff,e_outline:48/co_ffffff,e_outline:inner_fill:48/co_bbbbbb,e_outline:3:1000/c_mpad,g_center,h_1260,w_1260/b_rgb:eeeeee/c_limit,f_jpg,h_630,q_90,w_630/v1499403965/production/designs/1719844_1.jpg',
  'https://i.pinimg.com/originals/fc/f4/b8/fcf4b8f26313f02b0dd78ef58ab9db24.jpg',
  'http://retro-hd.com/uploads/img/article/affiche/6338-archer-revient-pour-une-huitieme-saison.jpeg',
  'https://78.media.tumblr.com/4856889dc6b16ec6d745f697aa63be30/tumblr_opi0yctKJB1w526eio1_500.png'
];


router.post('/insert-user', (req: Request, res: Response) => {
  // console.log(`body response ::`, req);

  const name = req.body.name;
  const profile_url = req.body.profile_url;
  const contact_number = req.body.contact_number;
  const email = req.body.email;
  const abv_count = req.body.abv_count;

  controller.createUser(name, profile_url, contact_number, email, abv_count)
      .catch((err: CustomError) => {
        console.log(chalk.red(`[/insert-user]   :: Error ${err.message}`));
        return Promise.reject(err);
      })
      .then((created_user) => {
        res.statusCode = 200;
        res.json({
          data: {
            message: `User entry successful`,
            created_data: created_user,
          }
        });
      })
      .catch((err: CustomError) => {
        res.statusCode = 409;
        res.json({
          errors: [
            {err: err.err, message: err.message}
          ]
        });
      });
});

router.post('/send-message', (req: Request, res: Response) => {

  const contact_number_to = req.body.contact_number_to;

  console.log(`Sending message to : ${contact_number_to}`);

  const message = 'Your OTP is ' + generateOTP() + req.body.message;
  const otp = generateOTP();
  const sent_message_from = req.body.sent_message_from;
  const sent_message_pic = profile_images[getRandomNumber(6, 1)];

  controller.sendMessage(contact_number_to, otp)
      .catch((err: CustomError) => {
        console.log(chalk.red(`[/insert-user]   :: Error ${err.message}`));
        return Promise.reject(err);
      })
      .then((sent_message) => {
        if (sent_message.success) {
          return controller.entry_sent_message(contact_number_to, sent_message_from, message, sent_message_pic)
              .then((created_doc) => {
                return Promise.resolve(created_doc);
              });
        } else {
          console.log(chalk.red(`failed to send message ::`));
        }
      })
      .then((sent_message) => {
        res.statusCode = 200;
        res.json({
          data: {
            message: `User entry successful`,
            created_data: sent_message,
          }
        });
      }).catch((err: CustomError) => {
    res.statusCode = 409;
    res.json({
      errors: [
        {err: err.err, message: err.message}
      ]
    });
  });

});


router.get('/all-users', (req: Request, res: Response) => {

  controller.getAllUser()
      .catch((err: CustomError) => {
        console.log(chalk.red(`[/all-users]   :: Error ${err.message}`));
        return Promise.reject(err);
      })
      .then((user_list) => {
        res.statusCode = 200;
        res.json({
          data: {
            message: `All users`,
            created_data: user_list,
          }
        });
      }).catch((err: CustomError) => {
    res.statusCode = 409;
    res.json({
      errors: [
        {err: err.err, message: err.message}
      ]
    });
  });

});

router.get('/all-sent-messages', (req: Request, res: Response) => {

  controller.getAllSentMessages()
      .catch((err: CustomError) => {
        console.log(chalk.red(`[/all-sent-messages]   :: Error ${err.message}`));
        return Promise.reject(err);
      })
      .then((all_sent_messages) => {
        res.statusCode = 200;
        res.json({
          data: {
            message: `All sent messages`,
            users_list: all_sent_messages,
          }
        });
      }).catch((err: CustomError) => {
    res.statusCode = 409;
    res.json({
      errors: [
        {err: err.err, message: err.message}
      ]
    });
  });

});


export default router;

