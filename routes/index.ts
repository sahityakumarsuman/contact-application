/**
 * Route layer.
 *
 * Route the API calls to controllers and send the
 * response back.
 */

import users from './users';
export {
  users
};

