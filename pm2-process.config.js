const DB_USERNAME = 'sahitya';
const DB_PASSWORD = 'sahitya123';
const DB_BASE_URL = `mongodb://${DB_USERNAME}:${DB_PASSWORD}@ds115553.mlab.com:15553/contact_application`;

const TWO_FACTOR_API_KEY = '8b4c597a-c4a3-11e8-a895-0200cd936042';


module.exports = {
  apps: [
    {
      name: 'contact-application',
      script: 'npm',
      args: 'start',
      watch: true,
      ignore_watch: ["node_modules", "dist"],
      env: {
        NODE_ENV: 'production',
        PORT: '8079',
        DB_USERNAME,
        DB_PASSWORD,
        DB_BASE_URL,
        TWO_FACTOR_API_KEY,
        TWO_FACTOR_API_KEY,
      }
    }
  ]
};

