import mongoose from 'mongoose';

import { IGuestModel, ISentMessageModel } from '../types/Models.d';

const Schema = mongoose.Schema;

const guestSchema = new Schema({
  name: {type: String, required: false},
  profile_url: {type: String, required: false},
  contact_number: {type: String, required: true, unique: true},
  email: {type: String, required: false},
  abv_count: {type: Number, required: false}
});

const sentMessageSchema = new Schema({
  sent_message_to: {type: String, required: false},
  sent_message_from: {type: String, required: false},
  message: {type: String, required: false},
  message_timestamp: {type: String, required: true},
  sent_message_pic: {type: String, required: false}
});


const Guest: mongoose.Model<IGuestModel> = mongoose.model<IGuestModel>('guests', guestSchema);

const SentMesssages: mongoose.Model<ISentMessageModel> = mongoose.model<ISentMessageModel>('sent_messages', sentMessageSchema);

export {
  Guest,
  guestSchema,
  SentMesssages,
  sentMessageSchema
};

