/**
 * Model layer.
 *
 * Interacts with MongoDB.
 */

const chalk = require('chalk');

const DB_CONN = require('../config/mongoose');
import { mongoDBEventEmitter } from '../utils/event-emitters';

import { Guest, guestSchema, sentMessageSchema, SentMesssages } from './Guest';

const utils = require('../utils/utils');
const constants = require('../utils/constants');

// initialization for the mongodb will go here...
setTimeout(() => mongoDBEventEmitter.emit('ready', '[mongoose] Connection with MongoDB done'), 1000);

export {
  Guest,
  guestSchema,
  SentMesssages,
  sentMessageSchema
};

