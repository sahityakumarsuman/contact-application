import mongoose from 'mongoose';
import chalk from 'chalk';

const DB_NAME = 'contact_application';

const PROD_CONFIG = {
  username: process.env.DB_USERNAME || 'sahitya',
  password: process.env.DB_PASSWORD || 'sahitya123',
  port: 27017,
  db: DB_NAME
};

const DEV_CONFIG = {
  username: 'admin',
  password: '',
  port: 27017,
  db: DB_NAME
};

const DEV_DB_CONN_URL = `mongodb://localhost:${DEV_CONFIG.port}/${DEV_CONFIG.db}`;

const PROD_DB_CONN_URL = `mongodb://${PROD_CONFIG.username}:${PROD_CONFIG.password}@ds115553.mlab.com:15553/contact_application`;

const DB_CONN_URL = process.env.NODE_ENV === 'production' ?
    PROD_DB_CONN_URL : DEV_DB_CONN_URL;

const CONN_CONF = {
  useMongoClient: true,
  reconnectTries: Number.MAX_VALUE,
  reconnectInterval: 500,
  bufferMaxEntries: 0
};

const connection = mongoose.connect(DB_CONN_URL, CONN_CONF, err => {
  console.log(DB_CONN_URL);
  if (err)
    console.log(chalk.red(`[mongoose] error connecting to mongodb: ${err.message}`)); // @ts-ignore
  else console.log(chalk.green('[mongoose] mongodb connected'));
});

mongoose.Promise = global.Promise;

module.exports = {
  connection
};

