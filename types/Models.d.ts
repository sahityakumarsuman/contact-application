import { Document } from 'mongoose';


export interface IGuestModel extends Document {
  _id: string;
  name: string;
  profile_url: string;
  contact_number: string;
  email: string;
  abv_count: number;
}

export interface ISentMessageModel extends Document {
  _id: string;
  sent_message_to: string;
  sent_message_from: string;
  message: string;
  message_timestamp: string;
  sent_message_pic: string;
}

