export interface CustomError extends Error {
    err: string;
    message: string;
}
